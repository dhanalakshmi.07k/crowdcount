/**
 * Created by dhanalakshmi on 7/11/16.
 */

var crowdConstant = {
    low:{
        maxCount:5,
        fillColor:'#5D8EC8'//blue
    },
    medium:{
        maxCount:7,
        fillColor:'#E0D899'//yellow
    },
    high:{
        fillColor:'#CA7235'//red
    }
}

module.exports={crowdConstant:crowdConstant};
