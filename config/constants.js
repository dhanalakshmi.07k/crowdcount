/**
 * Created by zendynamix on 7/4/2016.
 */

var constants = {
 SOCKET_DETAILS:{
        CROWD_DETECTION_DATA_PUSHER:'crowdDetectionNotifier',
     CROWD_DETECTION_AGGREGATED_PUSHER:'aggregatedCrowdData'
    }
}

module.exports={constants:constants};