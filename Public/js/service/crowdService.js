/**
 * Created by suhas on 5/11/16.
 */
NEC.factory("crowdService",function($http,$rootScope){
    var parkingData,chartConfig={};

var getCrowdHistoricalData = function(){
    return $http.get('/getCrowdHistoricalData')
}
    var getColorConfig = function(){
        return $http.get('/getCrowdColorConstant')
    }
    var setBarChartColorConfig=function (colorConfig) {
        chartConfig=colorConfig;

    }
    var getBarChartColorConfig=function () {
        return chartConfig
    }
    return{
        getCrowdHistoricalData:getCrowdHistoricalData,
        getColorConfig:getColorConfig,
        setBarChartColorConfig:setBarChartColorConfig,
        getBarChartColorConfig:getBarChartColorConfig
    }
})