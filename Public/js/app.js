/**
 * Created by MohammedSaleem on 11/11/15.
 */

var dependencies = ['ui.router', 'flashGraphUI', 'circularLoaderUI'];

var NEC = angular.module("NEC", dependencies);

NEC.run(function(crowdService){
    crowdService.getColorConfig()
        .then(function(result){
            crowdService.setBarChartColorConfig(result.data)
        }, function error(errResponse) {
            console.log("cannot get config")
        })
})


NEC.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state('admin', {
        url: "/admin",
        templateUrl: 'credentials/admin.html',
        controller: 'loginCtrl'
    })
        .state('admin.signIn', {
            url: "/signIn",
            templateUrl: 'credentials/signIn.html'
        })
        .state('admin.signUp', {
            url: "/signUp",
            templateUrl: 'credentials/signUp.html'
        })
        .state('app', {
            url: "/app",
            templateUrl: 'templates/app.html'
        })
        .state('app.dashboard', {
            url: "/dashboard",
            templateUrl: 'templates/dashboard.html',
            controller:"dashboardCtrl"
        })
        .state('app.settings', {
            url: "/settings",
            templateUrl: 'templates/settings.html'
        })
        .state('app.settings.generalSettings', {
            url: "/generalSettings",
            controller: "userCtrl",
            templateUrl: 'templates/settings/generalSettings.html'
        });

    $urlRouterProvider.otherwise("/app/dashboard");
});


NEC.directive('repeatDone', function () {
    return function (scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
});