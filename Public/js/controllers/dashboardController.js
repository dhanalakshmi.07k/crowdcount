/**
 * Created by dhanalakshmi on 5/11/16.
 */
NEC.controller('dashboardCtrl', function ($scope,crowdService) {
    $scope.dashboardCtrl={
        currentDate:new Date(),
        currentTime:"",
        crowdCountData:[],
        crowdCount:0
    }
    $scope.x=[
        '12:00',
        '13:00',
        '14:00',
        '15:00',
        '16:00',
        '17:00',
        '18:00',
        '19:00',
        '20:00',
        '21:00',
        '22:00',
        '23:00',
        '24:00',
        '01:00',
        '02:00',
        '03:00',
        '04:00'
    ];

    $scope.y=[{
        name: 'People Crowd',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        color:'#5D8EC8'
    }]



    var colorConfigObj=crowdService.getBarChartColorConfig();
    $scope.graphAdditional={
        tooltip: {
            formatter: function() {
                return 'Time: <b>' + this.x + '</b> <br> People Count: <b>' + this.y + '</b>';
            }
        },
        plotOptions: {
            column: {
                zones: [{
                        value: colorConfigObj.low.maxCount, // Values up to 4 (not including) ...
                        color: colorConfigObj.low.fillColor
                    },
                    {
                        value: colorConfigObj.medium.maxCount, // Values up to 7 (not including) ...
                        color:  colorConfigObj.low.fillColor
                    },
                    {
                    color:  colorConfigObj.high.fillColor
                }]
            }
        }
    };


   /* $scope.dashboardCtrl.currentTime=$scope.dashboardCtrl.currentDate.getHours()+":"+"0"+$scope.dashboardCtrl.currentDate.getMinutes()*/
    socket.on('crowdDetectionNotifier',function (data) {

        $scope.dashboardCtrl.crowdCountData.push(data)
        $scope.dashboardCtrl.crowdCount= data.PeopleCount
        $scope.$apply()
    });


    socket.on('aggregatedCrowdData',function (data) {
        updatingSeries(data);
    });


    function updatingSeries(data){

        var chartObject = getChartObject();
        if(chartObject){
            var chart = chartObject;
            var chartObj = chart.series[0];
            chartObj.addPoint([data.timeIndex, parseInt(data.crowdCountAvg.toFixed())], true, true);

        }



    }

    function getChartObject(){
        var chart = $("#container .flashGraph").highcharts();
        if(chart){
            return chart
        }else{
            return null
        }
    }
    function getGraphDetails(data){
        var chart = getChartObject();
        if (chart) {
            updateGraph(chart,data.crowdData,0);
            chart.xAxis[0].update({categories:data.timeIndex}, true);

        }
        else {
            console.log("chart not defined")
        }
    }
    function updateGraph(chartObject,dataSeries,index){
        var chart = chartObject;
        if(chart.series[index]){
            chart.series[index].setData(dataSeries);
            chart.redraw();
        }else{
        }
    }
    function getCrowdDataHistoricalData(){
        crowdService.getCrowdHistoricalData()
        .then(function(result){
            getGraphDetails(result.data)
        })
    }




    function updateTime(flag) {

        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
       /* s = checkTime(s);*/
        $scope.dashboardCtrl.currentTime =
            h + ":" + m ;
        if(flag!==1){
            $scope.$apply()
        }
        var t = setTimeout(updateTime, 500);

    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }
    updateTime(1);


    function init(){


        getCrowdDataHistoricalData()

    }
    init();
})