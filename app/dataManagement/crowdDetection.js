/**
 * Created by Suhas on 7/7/2016.
 */
var crowdDetectionModel = require('../models/crowdDetectionModel');
var socketConn = require('../../config/socketIo');
var constants = require('../../config/constants').constants;
var q = require('q');

var save=function(crowdData){

    var crowDataObj=JSON.parse(crowdData)
    var crowData = new crowdDetectionModel();
    crowData.PeopleCount=crowDataObj.PeopleCount;
    crowData.locationId=crowDataObj.locationId;
    crowData.timeStamp=crowDataObj.timestamp
    crowData.receivedTime=new Date()

    pushToSocket(crowDataObj)

    crowData.save(function(err,res){
        if(err){
            console.log(err.stack)
        }else{
            /*console.log(' Data Saved')*/
        }
    })
}

var pushToSocket=function(msg){
    /*msg.timeStamp=new Date(msg.timeStamp)*/
    socketConn.getSocketIoServer().emit(constants.SOCKET_DETAILS.CROWD_DETECTION_DATA_PUSHER,msg);
}

var crowdCountHistoricalData = function(startDate,series){
    return new Promise(function(resolve,reject){
        var aggregationStartDate = new Date(startDate)
        var aggregationEndDate = new Date(startDate)
        aggregationEndDate.setMinutes(aggregationEndDate.getMinutes()-series)
        var promises=[];

        var timeIndex = [];
        timeIndex[0]=getTimeFormat(aggregationStartDate);
        for(var i=0;i<60;i++){
            if(i==0){
            }else{
                aggregationStartDate.setMinutes(aggregationStartDate.getMinutes()-series)
                aggregationEndDate.setMinutes(aggregationEndDate.getMinutes()-series)
            }
            timeIndex[i+1]=getTimeFormat(aggregationEndDate);
            var promise=getCrowdCountHistoricalData(aggregationStartDate,aggregationEndDate).
            then(function(result){
                var crowdCount=0;
                if(result.length>0){
                    crowdCount=result[0].crowdCountAverage;
                }
                return q(crowdCount);
            })
            promises.push(promise)
        }
        q.all(promises).then(function(resultData){
            var data = {
                timeIndex:timeIndex.reverse(),
                crowdData:resultData.reverse()
            }
            resolve(data)
        })
    })

}
function getTimeFormat(Time){

    var minute = Time.getMinutes();
    var seconds = Time.getSeconds();
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    if (minute < 10) {
        minute = "0" + minute;
    }
    return Time.getHours() + ":" + minute;
}
function getCrowdCountHistoricalData(startDate, endDate){
    var deferred = q.defer();
    var aggregationStartDate1 = new Date(startDate)
    var aggregationEndDate1 = new Date(endDate)
    crowdDetectionModel.aggregate(
        [
            {
                $match: {
                    "receivedTime":{"$gt":aggregationEndDate1,"$lt":aggregationStartDate1}
                }
            },
            {
                $group: {
                    "_id" :null,
                    "crowdCountAverage":{"$avg" :"$PeopleCount"}
                }
            }

        ],function(err,result){
            if(err){
                console.log(err.stack)
            }else if(result && result.length>0){
                deferred.resolve(result);
            }else if(result && result.length==0){
                deferred.resolve(result);
            }

        });
    return deferred.promise;
}
var crowdAvgCountOverGivenMinute = function(minute){
    return new Promise(function(resolve,reject){
        var startDate = new Date();
        var endDate = new Date();
        endDate.setMinutes(startDate.getMinutes()-minute);
        getCrowdCountHistoricalData(startDate, endDate)
        .then(function(data){
            var value = {
                crowdCountAvg:data[0].crowdCountAverage,
                timeIndex:getTimeFormat(startDate)
            }
            resolve(value)
        })
    })
}
var pushCrowdAggregatedDataOverMinute = function(data){
    socketConn.getSocketIoServer().emit(constants.SOCKET_DETAILS.CROWD_DETECTION_AGGREGATED_PUSHER,data);
}
module.exports={
    save:save,
    pushToSocket:pushToSocket,
    crowdCountHistoricalData:crowdCountHistoricalData,
    crowdAvgCountOverGivenMinute:crowdAvgCountOverGivenMinute,
    pushCrowdAggregatedDataOverMinute:pushCrowdAggregatedDataOverMinute
}