/**
 * Created by suhas on 5/11/16.
 */
var zmq = require('zmq');
var zmqPortPart = 'tcp://127.0.0.1:6301';
var sock = zmq.socket('push');
sock.connect(zmqPortPart);
setInterval(function () {
    var data=
    {
        "PeopleCount":randomInt(40,200),
        "locationId": 1,
        "timestamp": new Date()
    }
    sock.send(JSON.stringify(data));
}, 2000);
function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}