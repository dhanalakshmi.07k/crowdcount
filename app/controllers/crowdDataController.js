/**
 * Created by suhas on 5/11/16.
 */
var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
var crowdManagement = require('../dataManagement/crowdDetection');
var crowdConstant= require('../../config/barChartColorConfig').crowdConstant;

module.exports = function (app) {
    app.use(router);
};


router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized Token Is not present');
    }
});


/*
router.get('/getData', function (req, res) {
    crowdAggregrator.crowdDetectionAggregation.countDataForLast15Minutes();
})*/
router.get('/getCrowdHistoricalData', function (req, res) {
    crowdManagement
    .crowdCountHistoricalData(new Date(),1)
    .then(function(data){
        res.send(data)
    })
})

router.get('/getCrowdHistoricalData/minutes/:minutes', function (req, res) {
    crowdManagement
        .crowdAvgCountOverGivenMinute(parseInt(req.params.minutes))
        .then(function(data){
            res.send(data)
        })
})

router.get('/getCrowdColorConstant', function (req, res) {
  res.send(crowdConstant)
})



setInterval(function(){
    crowdManagement
        .crowdAvgCountOverGivenMinute(1)
        .then(function(data){
            crowdManagement.pushCrowdAggregatedDataOverMinute(data)
        })
},60000)
