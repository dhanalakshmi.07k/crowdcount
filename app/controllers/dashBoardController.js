/**
 * Created by dhanalakshmi on 5/11/16.
 */
var express = require('express');
var mongoose = require('mongoose');
router = express.Router();
var cors = require('cors');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
var crowdAggregrator = require('../crowdDetectionAggregation');

module.exports = function (app) {
    app.use(router);
};


router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized Token Is not present');
    }
});


router.get('/getData', function (req, res) {
    crowdAggregrator.crowdDetectionAggregation.countDataForLast15Minutes();
})