/**
 * Created by Suhas on 7/7/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var crowdDetectionSchema = new mongoose.Schema(
    {
    PeopleCount:Number,
    locationId: Number,
    timeStamp: Date,
        receivedTime:Date

},{collection: "crowdDetection"})
module.exports = mongoose.model('crowdDetection',crowdDetectionSchema);


