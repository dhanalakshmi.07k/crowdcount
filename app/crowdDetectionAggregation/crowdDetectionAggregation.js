/**
 * Created by dhanalakshmi on 5/11/16.
 */

var mongoose = require('mongoose'),
    crowdDetectionModel = require("../models/crowdDetectionModel")
var socketConn = require('../../config/socketIo');
var constants = require('../../config/constants').constants;

var countDataForLast15Minutes=function(){


    var startDate = new Date()
    var endDate = new Date()
        startDate.setMinutes(startDate.getMinutes()-15)
    console.log(startDate)
    console.log(endDate)

    crowdDetectionModel.aggregate(
        [ {
            $match: {
                receivedTime: {$gt: startDate, $lt:endDate}
            }
        },
            {$sort: {_id: 1}},
            {
                $project: {
                    _id: 0,
                    PeopleCount: 1
                }
            }
        ],
        function (err, result) {
            if (err) {
                console.log(err);
                return;
            }
            console.log(result)
            socketConn.getSocketIoServer().emit(constants.SOCKET_DETAILS.CROWD_DETECTION_AGGREGATED_PUSHER,result.length);
        }   )


}
function startIntervalForRefreshingGraph() {
    /*var interval = setInterval(function () {
        countDataForLast15Minutes()
    },10000);*/
}
startIntervalForRefreshingGraph()



module.exports={
    countDataForLast15Minutes:countDataForLast15Minutes,

}
